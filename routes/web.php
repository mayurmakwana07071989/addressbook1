<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware('auth')->group(function () {
Route::get('logout', 'HomeController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::get('/addressbookadd', 'AddressbookController@create')->name('addressbook.add');
Route::post('/addressbookstore', 'AddressbookController@store')->name('addressbook.store');


Route::get('/addressbookedit/{id}', 'AddressbookController@edit')->name('addressbook.edit');
Route::post('/addressbookupdate/{id}', 'AddressbookController@update')->name('addressbook.update');
Route::get('/addressbookdelete/{id}', 'AddressbookController@delete')->name('addressbook.delete');

Route::get('/getState/{id}', 'AddressbookController@getState');
Route::get('/getCity/{id}', 'AddressbookController@getCity');

});