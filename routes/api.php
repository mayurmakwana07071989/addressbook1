<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::middleware('auth:web')->get('/', function (Request $request) {


	
     return $request->user();

});

	

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');


Route::post('addressadd', 'Api\AddressbooksController@addressadd');
	Route::post('addressupdate', 'Api\AddressbooksController@addressupdate');
	Route::get('addressdelete/{id}', 'Api\AddressbooksController@addressdelete');



