<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('country')->insert(
    			['name' => 'india']
		);

		DB::table('state')->insert(
    			['name' => 'gujarat','country_id'=>1]
		);
		DB::table('city')->insert(
    			['name' => 'ahmedabad','state_id'=>1],
    			['name' => 'rajkot','state_id'=>1]
		);

        // $this->call(UsersTableSeeder::class);
    }
}
