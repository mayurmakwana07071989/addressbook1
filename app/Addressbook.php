<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addressbook extends Model
{
    protected $table = 'addressbook';
    protected $fillable = ['country_id','state_id','city_id','zipcode','fulladdress'];
    public $timestamps = true;

    public function cityName(){
        return $this->belongsTo(City::Class,'city_id');
    }
    public function stateName(){
        return $this->belongsTo(State::Class,'state_id');
    }
    public function countryName(){
        return $this->belongsTo(Country::Class,'country_id');
    }
}
