<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Addressbook; 
use Illuminate\Support\Facades\Auth; 
use Validator;

class AddressbooksController extends Controller
{
    public $successStatus = 200;



    public function addressadd(Request $request) 
    { 


        $validator = Validator::make($request->all(), [ 
            'country_id' => 'required', 
            'state_id' => 'required',
            'city_id' => 'required',
            'zipcode' => 'required',
            'fulladdress' => 'required',
            
        ]);
       
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }else{
            $addressbook= Addressbook::create([

            'country_id'=>$request->country_id,
            'state_id'=>$request->state_id,
            'city_id'=>$request->city_id,
            'zipcode'=>$request->zipcode,
            'fulladdress'=>$request->fulladdress

        ]);
            return response()->json(['success' => $addressbook], $this-> successStatus);
        }

         
     
         
    } 

    public function addressupdate(Request $request){
          $validator = Validator::make($request->all(), [ 
            'country_id' => 'required', 
            'state_id' => 'required',
            'city_id' => 'required',
            'zipcode' => 'required',
            'fulladdress' => 'required',
            'rec_id' => 'required',
            
        ]);
       
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }else{
            $addressbook= Addressbook::where('id',$request->rec_id)->update([

            'country_id'=>$request->country_id,
            'state_id'=>$request->state_id,
            'city_id'=>$request->city_id,
            'zipcode'=>$request->zipcode,
            'fulladdress'=>$request->fulladdress

        ]);
            return response()->json(['success' => $addressbook], $this->successStatus);
        }

    } 

    public function addressdelete($id){      
       
    if (empty($id)) { 
            return response()->json(['error'=>'please enter id'], 401);            
        }else{
            
            $addressbook = Addressbook::where('id',$id)->delete();
            return response()->json(['success' => $addressbook], $this->successStatus);
        }
    } 
    


}
