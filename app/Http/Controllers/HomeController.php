<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Addressbook;
use App\User;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $addressbook = Addressbook::with(['cityName','stateName','countryName'])->latest();
        return view('home',compact('addressbook'));
    }

     public function  profile(){
         $userdata = User::find(Auth::user()->id);

         return view('profile',compact('userdata'));
     }

      public function logout(){
     
        return  Auth::logout();
    }
}
