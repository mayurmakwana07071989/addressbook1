<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\State;
use App\City;
use App\Addressbook;
class AddressbookController extends Controller
{
    

    public function create()
    {
    	$country = Country::get();    	
 		return view('addressbook.create',compact('country'));   	
    }

    public function store(Request $request)
    {

    	Addressbook::create([

    		'country_id'=>$request->country_id,
    		'state_id'=>$request->state_id,
    		'city_id'=>$request->city_id,
    		'zipcode'=>$request->zipcode,
    		'fulladdress'=>$request->fulladdress

    ]);	
 		return redirect('home');   	
    }
    

    public function edit($id)
    {
    	$country = Country::get();
    	$state = State::get();
    	$city = City::get(); 
    	$addressbook = Addressbook::where('id',$id)->first();
 		return view('addressbook.edit',compact('addressbook','country','city','state'));   	
    }

	public function update(Request $request,$id)
    {

    Addressbook::where('id',$id)->update([
    		'country_id'=>$request->country_id,
    		'state_id'=>$request->state_id,
    		'city_id'=>$request->city_id,
    		'zipcode'=>$request->zipcode,
    		'fulladdress'=>$request->fulladdress

    ]);	
    return redirect('home');

	}

	public function delete($id)
    {
    	 Addressbook::where('id',$id)->delete();
    	return redirect('home');
    }
    


    public function getState($id){

    	$html ='';

    	$state = State::where('country_id',$id)->get();
    	foreach ($state as $key => $value) {
    		$html.='<option value="">Select State</option><option value="'.$value->id.'">'.$value->name.'</option>';
    	}

    	return $html;
    }

    public function getCity($id){

    	$html ='';

    	$city = City::where('state_id',$id)->get();
    	foreach ($city as $key => $value) {
    		$html.='<option value="">Select City</option><option value="'.$value->id.'">'.$value->name.'</option>';
    	}

    	return $html;
    }
}
