@extends('layouts.app')
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{ route('addressbook.add') }}">Add Address</a>
<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Country</th>
                <th>State</th>
                <th>City</th>
                <th>Zipcode</th>
                <th>Full Address</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>

            @foreach($addressbook->get() as $ab)
            <tr id="tr{{ $ab->id }}">
                <td>{{ $ab->countryName->name }}</td>
                <td>{{ $ab->stateName->name }}</td>
                <td>{{ $ab->cityName->name }}</td>
                <td>{{ $ab->zipcode }}</td>
                <td>{{ $ab->fulladdress }}</td>
                <td><a href="{{ route('addressbook.edit',['id'=>$ab->id]) }}">Edit</a> &nbsp;&nbsp;<a class="delete"  data-id="{{ $ab->id }}">Delete</a></td>
            </tr>

            @endforeach
           
        </tbody>
        <tfoot>
            <tr>
               <th>Country</th>
                <th>State</th>
                <th>City</th>
                <th>Zipcode</th>
                <th>Full Address</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>



                </div>
            </div>
        </div>
    </div>
</div>





@endsection
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
<script type="text/javascript">
    $(document).ready(function(){


        $(".delete").on('click',function(){
            var rec_id = $(this).data('id');
           
    $.ajax({ method: "GET", data: { 'rec_id':rec_id,'_token':"{{ csrf_token() }}" },contentType: false,
  processData: false,
  cache: false,url: "{{ url('/api/addressdelete') }}/"+rec_id, success: function(result){
               
        alert('address delete successfully');                          
        $("#tr"+rec_id).remove();
        
    },error: function (request, status, error) {
           alert('error while update record');
            //.......
        }

    });

  });
});
    
    

</script>