<html>
  <head>
    <meta charset="utf-8">
    <title>Demo</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>  
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

    <style>
        .error{
            color:red;
        }
    </style>
  </head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form id="form">
                      

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="button" class="login btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
   
    $(".login").on('click',function(){
    if($("#form").valid()){  

        
        var email = $("#email").val();
        
        var password = $("#password").val();
        

        var formData = new FormData();
      
        formData.append('email', email);
        formData.append('password', password);
        formData.append('_token', "{{ csrf_token() }}");
  

    $.ajax({ method: "POST", data: formData,contentType: false,
  processData: false,
  cache: false,url: "{{ url('/api/login') }}", success: function(result){
               
               alert('login success');           
               
               location.href="home";
            
                    
               


    },error: function (request, status, error) {
           alert('Please enter valid credential');
            //.......
        }

    });
}
  });

</script>



<script>
 
    $(document).ready(function () {
 
    $('#form').validate({ // initialize the plugin
        rules: {            
            email: {
                required: true,
                email: true
            },
            number: {
                required: true,
                digits: true
            },
            minlength: {
                required: true,
                minlength: 5
            },
            maxlength: {
                required: true,
                maxlength: 8
            },
            minvalue: {
                required: true,
                min: 1
            },
            maxvalue: {
                required: true,
                max: 100
            }
           
        }
    });
});
</script>
 
</body>
 
</html>

