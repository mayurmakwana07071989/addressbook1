<html>
  <head>
    <meta charset="utf-8">
    <title>Demo</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>  
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

    <style>
        .error{
            color:red;
        }
    </style>
  </head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form id="form" >
                      

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mobile" class="col-md-4 col-form-label text-md-right">
                                Mobile
                            </label>

                            <div class="col-md-6">
                                <input maxlength="10" id="mobile" type="number" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile">

                                @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                    <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">
                                Gender
                            </label>

                            <div class="col-md-6">
                                <input id="male" type="radio" class="gender @error('gender') is-invalid @enderror" name="gender" value="m" checked="" autocomplete="gender">Male &nbsp;

                                 <input id="female" type="radio" class="gender @error('gender') is-invalid @enderror" name="gender" value="f"  autocomplete="gender">Female &nbsp;
                                  <input id="other" type="radio" class="gender @error('gender') is-invalid @enderror" name="gender" value="o"  autocomplete="gender">Other


                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                         <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label text-md-right">
                                Image
                            </label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" required autocomplete="image">

                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="hobbies" class="col-md-4 col-form-label text-md-right">{{ __('hobbies') }}</label>

                            <div class="col-md-6">
                                <input id="hobbies" type="text" class="form-control @error('hobbies') is-invalid @enderror" name="hobbies" value="{{ old('hobbies') }}" required autocomplete="hobbies" autofocus>

                                @error('hobbies')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                         

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="button" class="submit1 btn btn-primary">
                                    {{ __('Register') }}
                                </button>

                                <a href="{{ route('home') }}">
                                 <button type="button" class="btn btn-primary">
                                 Cancel
                                </button>
                                </a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
   
    $(".submit1").on('click',function(){
    if($("#form").valid()){  

        var name = $("#name").val();

        var email = $("#email").val();
        var mobile = $("#mobile").val();
        var gender = $(".gender").val();
        var hobbies = $("#hobbies").val();
        var password = $("#password").val();
        var password_confirmation = $("#password-confirm").val();

        var Img = $('input[name="image"]').get(0).files[0];
        var formData = new FormData();
        formData.append('image', Img);        
        formData.append('name', name);
        formData.append('email', email);
        formData.append('mobile', mobile);
        formData.append('gender', gender);
        formData.append('hobbies', hobbies);

        formData.append('password', password);
        formData.append('c_password', password_confirmation);

    $.ajax({ method: "POST", data: formData,contentType: false,
  processData: false,
  cache: false,url: "{{ url('/api/register') }}",headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}" 
               }, success: function(result){
               
               alert('register success');
               location.href="login";
                    
               


    },error: function (request, status, error) {
           alert('error while register please check details');
            //.......
        }

    });
}
  });

</script>



<script>
 
    $(document).ready(function () {
 
    $('#form').validate({ // initialize the plugin
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            number: {
                required: true,
                digits: true
            },
            minlength: {
                required: true,
                minlength: 5
            },
            maxlength: {
                required: true,
                maxlength: 8
            },
            minvalue: {
                required: true,
                min: 1
            },
            maxvalue: {
                required: true,
                max: 100
            },
            range: {
                required: true,
                range: [20, 40]
            },
            url: {
                required: true,
                url: true
            },
            filename: {
                required: true,
                extension: "jpeg|png"
            },
        }
    });
});
</script>
 
</body>
 
</html>

