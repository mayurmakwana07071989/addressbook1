<html>
  <head>
    <meta charset="utf-8">
    <title>Demo</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>  
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

    <style>
        .error{
            color:red;
        }
    </style>
  </head>
<body>


<form id="form">
	

	Country Name: 
	<select name="country_id" id="country_id" required="">
		<option value="">Select</option>

		@foreach($country as $country)
		<option value="{{ $country->id }}">{{ $country->name }}</option>
		@endforeach

	</select>
	<br>

	State Name:
	<select name="state_id" id="state_id" required="">
		<option value="">Select</option>

	</select>
	<br>

	City Name:
	<select name="city_id" id="city_id" required="">
		<option value="">Select</option>

	</select>
	<br>
	Zip code:
	<input type="number" value="" id="zipcode" name="zipcode" class="numeric" required="">
	<br>
	Full Address :
	<textarea name="fulladdress" id="fulladdress" required=""></textarea>
	<br>
	<input id="addaddress" type="button" name="submit" value="Save"> <a href="{{ route('home')}}"><button type="button">Cancel</button></a>
</form>

<script type="text/javascript">
		
$(document).ready(function(){

  $("#country_id").on('change',function(){
   
  	var cid = $("#country_id").val();	
    $.ajax({url: "{{ url('getState') }}/"+cid, success: function(result){
      $("#state_id").html(result);
    }});
  });


  $("#state_id").on('change',function(){
  	var sid = $("#state_id").val();
    $.ajax({url: "{{ url('getCity') }}/"+sid, success: function(result){
      $("#city_id").html(result);
    }});
  });

});
 







 
    
 $(document).ready(function(){


 	    $("#addaddress").on('click',function(){
    if($("#form").valid()){  

        
        var country_id = $("#country_id").val();        
        var state_id = $("#state_id").val();
        var city_id = $("#city_id").val();
        var zipcode = $("#zipcode").val();
        var fulladdress = $("#fulladdress").val();
        var formData = new FormData();
      
        formData.append('country_id', country_id);
        formData.append('state_id', state_id);
        formData.append('city_id', city_id);
        formData.append('zipcode', zipcode);
        formData.append('fulladdress', fulladdress);
        formData.append('_token', "{{ csrf_token() }}");
  

    $.ajax({ method: "POST", data: formData,contentType: false,
  processData: false,
  cache: false,url: "{{ url('/api/addressadd') }}", success: function(result){
               
               alert('address added successfully');           
               
               location.href="{{ url('home') }}";
            
                    
               


    },error: function (request, status, error) {
           alert('error while create record');
            //.......
        }

    });
}
  });


$('#form').validate({ // initialize the plugin
        rules: {            
            email: {
                required: true,
                email: true
            },
            number: {
                required: true,
                digits: true
            },
            minlength: {
                required: true,
                minlength: 6
            },
            maxlength: {
                required: true,
                maxlength: 8
            },
            minvalue: {
                required: true,
                min: 1
            },
            maxvalue: {
                required: true,
                max: 100
            }
           
        }
    });    
    });   
</script>
 
</body>
 
</html>